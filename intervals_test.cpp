// Intervals_test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

// TODO: Benchmarks

#include <iostream>
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "interval_map.h"

class my_number
{
private:
	int m_ii{};
public:
	explicit my_number(const int ii)
		: m_ii(ii)
	{}
	~my_number()
	{
		//std::cout << "my_number: '" << m_ii << "' destructed.\n";
	}

	bool operator <(const my_number& other) const
	{
		return m_ii < other.m_ii;
	}

	my_number(const my_number& other) = default;

	my_number& operator=(const my_number& other)
	{
		if (this == &other)
			return *this;
		m_ii = other.m_ii;
		return *this;
	}

	my_number(my_number&& other) noexcept = delete;
	my_number& operator=(my_number&& other) noexcept = delete;
};

class my_char
{
private:
	char m_cc;
public:
	//explicit my_char(const char c)
	//	: m_cc(c)
	//{}
	explicit my_char(char&& c)
		: m_cc(c)
	{}
	~my_char()
	{
		//std::cout << "my_char: '" << m_cc << "' destructed.\n";
	}

	friend bool operator==(const my_char& lhs, const my_char& rhs)
	{
		return lhs.m_cc == rhs.m_cc;
	}

	friend bool operator!=(const my_char& lhs, const my_char& rhs)
	{
		return !(lhs == rhs);
	}

	my_char(const my_char& other) = default;


	my_char& operator=(const my_char& other)
	{
		if (this == &other)
			return *this;
		m_cc = other.m_cc;
		return *this;
	}

	my_char(my_char&& other) noexcept = default;
	my_char& operator=(my_char&& other) noexcept = default;
};

TEST_CASE("Can declare interval_map<int, char>", "[Can_declare_int_char]")
{
	interval_map<int, char> int_map('A');
}

// Test only specificated requirements of T and V are needed.
TEST_CASE("Can declare interval_map<my_number, my_char>", "[Empty_map]")
{
	interval_map<my_number, my_char> my_number_map(my_char{'A'});
}

TEST_CASE("Test empty map returns default value", "[Empty_map]")
{
	interval_map<int, char> int_map('A');
	REQUIRE(std::vector<char>{'A'} == int_map[0]);
	REQUIRE(std::vector<char>{'A'} == int_map[1]);
	REQUIRE(std::vector<char>{'A'} == int_map[100]);
	REQUIRE(std::vector<char>{'A'} == int_map[-1]);

}

TEST_CASE("Smoke test", "[Smoke_test]")
{
	interval_map<int, char> int_map('A');
	int_map.insert(10, 0, 'B');
	int_map.insert(0, 5, 'B');
	int_map.insert(5, 10, 'C');
	REQUIRE(std::vector<char>{'A'} ==  int_map[-1]);
	REQUIRE(std::vector<char>{'A', 'B'} == int_map[0]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[5]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[9]);
	REQUIRE(std::vector<char>{'A'} == int_map[10]);
	REQUIRE(std::vector<char>{'A'} == int_map[11]);
}

TEST_CASE("Single interval", "[Single_interval]")
{
	interval_map<double, char> int_map('A');
	int_map.insert(0.1, 5, 'B');
	REQUIRE(std::vector<char>{'A'} == int_map[0]);
	REQUIRE(std::vector<char>{'A', 'B'} == int_map[0.1]);
	REQUIRE(std::vector<char>{'A', 'B'} == int_map[1]);
	REQUIRE(std::vector<char>{'A'} == int_map[5]);
	REQUIRE(std::vector<char>{'A'} == int_map[1'000]);
}

TEST_CASE("Invalid interval", "[invalid_interval]")
{
	interval_map<double, char> int_map('A');
	int_map.insert(5, 0.1, 'B');
	REQUIRE(std::vector<char>{'A'} == int_map[0]);
	REQUIRE(std::vector<char>{'A'} == int_map[0.1]);
	REQUIRE(std::vector<char>{'A'} == int_map[1]);
	REQUIRE(std::vector<char>{'A'} == int_map[5]);
	REQUIRE(std::vector<char>{'A'} == int_map[1'000]);
}

TEST_CASE("Two intervals", "[two_intervals]")
{
	interval_map<int, char> int_map('A');
	int_map.insert(1, 5, 'B');
	int_map.insert(7, 9, 'C');
	REQUIRE(std::vector<char>{'A'} == int_map[0]);
	REQUIRE(std::vector<char>{'A', 'B'} == int_map[1]);
	REQUIRE(std::vector<char>{'A'} == int_map[5]);
	REQUIRE(std::vector<char>{'A'} == int_map[6]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[7]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[8]);
	REQUIRE(std::vector<char>{'A'} == int_map[9]);
	REQUIRE(std::vector<char>{'A'} == int_map[1'000]);
}

TEST_CASE("Two intervals_continuous", "[two_intervals_continuous]")
{
	interval_map<int, char> int_map('A');
	int_map.insert(1, 5, 'B');
	int_map.insert(5, 9, 'C');
	REQUIRE(std::vector<char>{'A'} == int_map[0]);
	REQUIRE(std::vector<char>{'A', 'B'} == int_map[1]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[5]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[6]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[7]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[8]);
	REQUIRE(std::vector<char>{'A'} == int_map[9]);
	REQUIRE(std::vector<char>{'A'} == int_map[1'000]);
}

TEST_CASE("Two intervals insert reversed", "[two_intervals_insert_reversed]")
{
	interval_map<int, char> int_map('A');
	int_map.insert(5, 9, 'C');
	int_map.insert(1, 5, 'B');
	REQUIRE(std::vector<char>{'A'} == int_map[0]);
	REQUIRE(std::vector<char>{'A', 'B'} == int_map[1]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[5]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[6]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[7]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[8]);
	REQUIRE(std::vector<char>{'A'} == int_map[9]);
	REQUIRE(std::vector<char>{'A'} == int_map[1'000]);
}

TEST_CASE("Three intervals", "[three_intervals]")
{
	interval_map<int, char> int_map('A');
	int_map.insert(1, 3, 'B');
	int_map.insert(5, 8, 'C');
	int_map.insert(12, 16, 'D');
	REQUIRE(std::vector<char>{'A'} == int_map[0]);
	REQUIRE(std::vector<char>{'A', 'B'} == int_map[1]);
	REQUIRE(std::vector<char>{'A'} == int_map[3]);
	REQUIRE(std::vector<char>{'A'} == int_map[4]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[5]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[6]);
	REQUIRE(std::vector<char>{'A', 'C'} == int_map[7]);
	REQUIRE(std::vector<char>{'A'} == int_map[8]);
	REQUIRE(std::vector<char>{'A'} == int_map[9]);
	REQUIRE(std::vector<char>{'A', 'D'} == int_map[12]);
	REQUIRE(std::vector<char>{'A', 'D'} == int_map[13]);
	REQUIRE(std::vector<char>{'A'} == int_map[16]);
	REQUIRE(std::vector<char>{'A'} == int_map[1'000]);
}