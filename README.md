A data structure containing a number of intervals. And to be able to effectively build it and mainly query: Given a value what intervals it's in.

TODO: Given an interval. what intervals it intersect or are contained in.

Included code was created in little less than a day. So taking into account day is not only coding it can demonstrate a day's work.
Of course changing the existing larger code base would be a bit of a different and more laborious process. It's needed to specify and agree on an interface, run tests of the rest of the code, consider and accommodate different use cases.

To developers reviewing the code:
Formatting: I always adhere to existing naming and formatting rules. As shown in the book Coding Complete 2,  the main quality of formatting is consistency.
Other guidelines: I'm trying to keep Core Guidelines.
Safety: No formal safety guidelines were used.

Platform:
My C++ knowledge evolves around standard C++. Most of the time my code was compiled by all three. (clang, gcc, MSVC)
This code is compiled and unit tested via clang 15.0 and msvc v19 as this is what I have in hand right now.
