#pragma once
#include <map>
#include <span>
#include <vector>

/// Interval map. Allows multiple intervals to be mapped to the same value.
/// The intervals are allowed to be overlapping and contiguous.
/// At the beginning whole key space is mapped to the initial value.
/// Search speed is O(log N) where N is the number of intervals.
/// Insertion speed is O(log N + M * S) where M is the number of intervals affected by the insertion
/// and S is number of values in the interval.
/// Requirements for K: operator < is defined. Assignable. Copyable.
/// for V: operator == is defined. Assignable, Copyable, Moveable.
/// TODO use std::less<K> and std::equals<V> instead of operator < and operator ==
/// TODO use C++ 20 modules instead of #pragma once and this header file
template <typename K, typename V>
class interval_map {
	friend void interval_map_test();
	std::vector<V> m_valDefault;
	std::map<K, std::vector<V>> m_map;
public:
	// constructor associates whole range of K with val
	explicit interval_map(V val)
	{
		m_valDefault.push_back(std::move(val));
	}

	// look-up of the value associated with key
	const std::vector<V>& operator[](K const& key) const
	{
		auto it = m_map.lower_bound(key);
		// out of bound of existing intervals in m_map
		if(m_map.empty())
		{
			return m_valDefault;
		}
		if (it == m_map.end())
		{
			return (--it)->second;
		}
		if (it == m_map.begin() && key < it->first) 
		{
			return m_valDefault;
		}
		// found in m_map
		if (key < it->first )
		{	// no equivalent found, key belongs to the previous interval
			auto& found = (--it)->second;
			return found;
		}
		// equivalent key found
		return it->second;
	}

	friend bool operator!=(const interval_map& lhs, const interval_map& rhs)
	{
		return lhs.m_map == rhs.m_map;
	}
	friend bool operator!=(const std::map<int, char>& lhs, const interval_map& rhs)
	{
		return lhs != rhs.m_map;
	}

	// Assign value val to interval [low, high).
	// When inserting interval where low == high, nothing is inserted.
	// When inserting interval where low > high, nothing is inserted.
	void insert(const K& low, const K& high, const V&& val)
	{
		if (low >= high)
		{
			return;
		}
		const V& default_value = *m_valDefault.begin();
		if(m_map.empty())
		{
			m_map.insert(m_map.end(), {low, std::vector<V>{default_value, val}});
			m_map.insert(m_map.end(), {high, {default_value}});
			return;
		}
		auto it = m_map.lower_bound(low);
		if (it == m_map.end())
		{
			m_map.insert(m_map.end(), {low, std::vector<V>{default_value, val}});
		}
		else if (low < it->first)
		{
			m_map.insert(it, {low, std::vector<V>{default_value, val}});
		}
		for (; it != m_map.end() && it->first < high; ++it)
		{
			auto& values_found = it->second;
			if (std::ranges::find(values_found, val) == values_found.end())
			{
				values_found.push_back(val);
			}
		}
		if (it == m_map.end())
		{
			m_map.insert(m_map.end(), {high, {default_value}});
		}
	}
};
